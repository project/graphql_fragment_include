<?php

namespace Drupal\graphql_fragment_include\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Graphql Fragment Include Configuration form class.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'graphql_fragment_include_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('graphql_fragment_include.settings');

    $form['fragments_base_dir'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Fragments base directory (relative to %drupal_root)", ['%drupal_root' => DRUPAL_ROOT]),
      '#required' => TRUE,
      '#description' => $this->t("It must start with a leading slash. Path to the base directory where your GraphQL Fragments are located, e.g.- /sites/default/graphql/fragments"),
      '#default_value' => $config->get('fragments_base_dir'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $previewPath = $form_state->getValue('fragments_base_dir');
    if (substr($previewPath, 0, 1) != '/') {
      $form_state->setErrorByName(
        'fragments_base_dir',
        $this->t('Fragments base directory must start with a leading slash.')
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('graphql_fragment_include.settings');
    $config->set('fragments_base_dir', $form_state->getValue('fragments_base_dir'));
    $config->save();

    return parent::submitForm($form, $form_state);
  }

  /**
   * Return the configuration names.
   */
  protected function getEditableConfigNames() {
    return [
      'graphql_fragment_include.settings',
    ];
  }

}
