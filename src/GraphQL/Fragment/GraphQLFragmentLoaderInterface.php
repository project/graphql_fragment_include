<?php

namespace Drupal\graphql_fragment_include\GraphQL\Fragment;

/**
 * Provides an interface for loading GraphQL Fragments.
 */
interface GraphQLFragmentLoaderInterface {

  /**
   * Find any line defining a fragment include.
   *
   * Returns an array  obtained from executing a regexp. This method should be
   * overridden if include format needs to be changed.
   *
   * @param string $graphQLQuery
   *   The GraphQL string to process.
   *
   * @return array
   *   The matches array obtained from executing a regexp.
   */
  public function findFragments(string $graphQLQuery);

  /**
   * Find and load GraphQL fragments.
   *
   * They could be defined as:
   * # include Fragment.gql
   * Other implementations of this interface could define another format for
   * fragment inclusion.
   *
   * @param string $graphQLQuery
   *   The GraphQL string to process.
   *
   * @return string
   *   The processed string.
   */
  public function loadFragments(string $graphQLQuery);

}
