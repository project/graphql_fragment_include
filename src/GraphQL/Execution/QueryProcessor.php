<?php

namespace Drupal\graphql_fragment_include\GraphQL\Execution;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\Context\CacheContextsManager;
use Drupal\graphql\GraphQL\Execution\QueryProcessor as BaseQueryProcessor;
use Drupal\graphql\Plugin\SchemaPluginManager;
use Drupal\graphql_fragment_include\GraphQL\Fragment\GraphQLFragmentLoaderInterface;
use GraphQL\Server\OperationParams;
use GraphQL\Server\ServerConfig;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * QueryProcessor service overriding BaseQueryProcessor.
 */
class QueryProcessor extends BaseQueryProcessor {

  /**
   * GraphQL fragment loader.
   *
   * @var \Drupal\graphql_fragment_include\GraphQL\Fragment\GraphQLFragmentLoaderInterface
   */
  protected $graphQLFragmentLoader;

  /**
   * Processor constructor.
   *
   * @param \Drupal\Core\Cache\Context\CacheContextsManager $contextsManager
   *   The cache contexts manager service.
   * @param \Drupal\graphql\Plugin\SchemaPluginManager $pluginManager
   *   The schema plugin manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   The cache backend for caching query results.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   * @param \Drupal\graphql_fragment_include\GraphQL\Fragment\GraphQLFragmentLoaderInterface $graphQLFragmentLoader
   *   The GraphQL fragment loader.
   */
  public function __construct(
    CacheContextsManager $contextsManager,
    SchemaPluginManager $pluginManager,
    CacheBackendInterface $cacheBackend,
    RequestStack $requestStack,
    GraphQLFragmentLoaderInterface $graphQLFragmentLoader
  ) {
    parent::__construct($contextsManager, $pluginManager, $cacheBackend, $requestStack);
    $this->graphQLFragmentLoader = $graphQLFragmentLoader;
  }

  /**
   * Preprocesses and execute a single query.
   *
   * @param \GraphQL\Server\ServerConfig $config
   *   ServerConfig.
   * @param \GraphQL\Server\OperationParams $params
   *   OperationParams.
   *
   * @return mixed
   *   The result.
   */
  public function executeSingle(ServerConfig $config, OperationParams $params) {
    // Load GraphQL fragments.
    if ($params->query) {
      $params->query = $this->graphQLFragmentLoader->loadFragments($params->query);
    }

    return parent::executeSingle($config, $params);
  }

}
