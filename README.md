# GraphQL fragment include

A module to include fragments in a GraphQL query.

## INTRODUCTION ##
This module is useful when:
* You have a collection of paragraphs (or any other kind of entity) that is
being used by multiple Content Types.
* GraphQL is used to export content data (ideally by using
[Static Suite](https://www.drupal.org/project/static_suite) module) and you
find the same data structure repeated on multiple queries.

To avoid having to repeat the same query for every content type, you should:
* use GraphQL fragments
* extract them to files
* use this module to include those fragment files into your GraphQL queries.

This module transforms the following:
```graphql
# include Image.gql
{
  content:nodeById(id: "1") {
    id: entityId
    ... on NodeArticle {
      title
      image: fieldImage {
        entity {
          ...Image
        }
      }
    }
  }
}
```

into this:

```graphql
# sample content from Image.gql
fragment Image on MediaImage {
  entityLabel
  ... on MediaImage {
    mediaImage: fieldMediaImage {
      alt
      title
      url
      width
      height
    }
    credit: fieldImageCredit
    caption: fieldImageCaption
  }
}

{
  content:nodeById(id: "1") {
    id: entityId
    ... on NodeArticle {
      title
      image: fieldImage {
        entity {
          ...Image
        }
      }
    }
  }
}
```

## CONFIGURATION ##
* Configure `fragments base directory` (/admin/config/graphql/fragment-include).
That is were your fragment files are located. It's usually a directory inside
`/sites/default/`.
* Create a file inside the above directory, with the contents of your fragment,
and with .gql extension. You can create subdirectories and include a fragment
inside another fragment (infinite recursion protection is available).
* Add an include to a GraqhQL query, using the following format:
  `# include {PATH_TO_FILE_INSIDE_FRAGMENTS_BASE_DIR}.gql`
* Execute the query, and the contents from the fragment file will be appended
to the query before execution.

## REQUIREMENTS ##
This modules depends on GraphQL module.

## INSTALLATION ##
Run `composer require drupal/graphql_fragment_include`.

## DEBUGGING ##
If a fragment can not be found, a warning message is logged.
Use [dblog](https://www.drupal.org/docs/8/core/modules/dblog) to view them.

## CAVEATS ##
* Due to the fact that the
[GraphQL specification](https://graphql.github.io/graphql-spec/) does not
support any kind of includes, we use comments and a "# include path.gql" syntax
that is completely custom. You can change that syntax to your convenience,
extending `graphql_fragment_include.graphql_fragment_loader` service.
* For the same above reason, [GraphiQL IDE](https://github.com/graphql/graphiql)
will remove the above comments when clicking "Prettify" button.

## TODO ##
* Find a way to maintain fragment includes when clicking GraphiQL's "Prettify"
button.
