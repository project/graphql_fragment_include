<?php

namespace Drupal\Tests\graphql_fragment_include\Kernel;

use Drupal\graphql_fragment_include\GraphQL\Fragment\GraphQLFragmentLoader;
use Drupal\KernelTests\KernelTestBase;

/**
 * GraphQL fragment include test class.
 *
 * @requires module drupal:graphql
 * @requires module drupal:graphql_core
 * @group graphql_fragment_include
 */
class GraphQLFragmentIncludeTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'graphql',
    'graphql_core',
    'graphql_fragment_include',
    'graphql_fragment_include_test',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $moduleHandler = \Drupal::service('module_handler');
    $modulePath = $moduleHandler->getModule('graphql_fragment_include_test')->getPath();
    $this->config('graphql_fragment_include.settings')
      ->set('fragments_base_dir', "/" . $modulePath . "/mocks/fragments")
      ->save();
  }

  /**
   * Tests tokens are replaced.
   */
  public function testFragment() {
    $configFactory = \Drupal::service('config.factory');
    $loggerFactory = \Drupal::service('logger.factory');
    $fragmentLoader = new GraphQLFragmentLoader($configFactory, $loggerFactory);

    $moduleHandler = \Drupal::service('module_handler');
    $modulePath = $moduleHandler->getModule('graphql_fragment_include_test')->getPath();
    $query = file_get_contents($modulePath . "/mocks/Query.gql");
    $query = $fragmentLoader->loadFragments($query);

    // Assert fragments are not repeated.
    $this->assertEquals(substr_count($query, "fragment Image on File"), 1);
    $this->assertEquals(substr_count($query, "fragment Video on File"), 1);
  }

}
